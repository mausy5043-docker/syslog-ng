# docker-syslog-ng

It is advised to clone the [Git repository](https://gitlab.com/mausy5043-docker/syslog-ng.git) and build your own image from that.

## Installing
The preferred procedure is:
```
git clone https://gitlab.com/mausy5043-docker/syslog-ng.git
cd syslog-ng
./build.sh Dockerfile.<distro>
```
Review the default settings in `${HOME}/.config/docker/syslog-ng.conf`. Then,

```
./run.sh
```

Replace the `<distro>` above by one of the supported distributions (`arch` (Arch Linux), `debian` (Debian) or `alpine` (Alpine Linux)).
Note: For Raspberry Pi 3 and newer building with `Dockerfile.debian` may be succesful (not tested).

The default configuration file assumes that the container has the folder `/srv/syslog-ng` available which is the base directory where all logs are stored by syslog-ng. Adjust the `${HOME}/.config/docker/syslog-ng.conf` file and `run.sh` if you want to use another directory.


## Build a fresh image

```
./build.sh Dockerfile.<distro>
```
This builds the image for distribution `<distro>`.
When the build completes succesfully you should review the settings in `${HOME}/.config/docker/syslog-ng.conf`.

Next `${HOME}/.config/docker/syslog-ng_on_<host>-config.txt` should be reviewed. This contains a line `store=/tmp/syslog-ng`. Which is the location on the host where the container will store the log files. Change the `/tmp/syslog-ng` to something sensible for your system.

## Run the image

```
./run.sh
```
This runs the container using the image you just built.
It uses a custom `syslog-ng.conf` that is kept in `${HOME}/.config/docker/syslog-ng.conf` as the configuration for the syslog-ng daemon.


## Stop a running container

```
./stop.sh
```
Stops the container and then deletes it. This allows for immediately running a container with the same name without the need to `docker rm` it manually.


## Updating
FIRST!! Make a safety-copy of `${HOME}/.config/docker/syslog-ng.conf` for your own mental health.

```
./update.sh [--all]
```
This force-pulls the current versions of all the files from the remote git repository. Use the `--all` switch to also rebuild the image and restart the container immediately afterwards.
Be aware that this will overwrite/delete any changes you may have made to the files in this folder!

DISCLAIMER:
Use this software at your own risk! We take no responsibility for ANY data loss.
We guarantee no fitness for any use specific or otherwise.
